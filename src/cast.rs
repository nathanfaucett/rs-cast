

pub trait Cast<T> {
    fn cast(self) -> T;
}

macro_rules! trait_cast {
    ($F:ty, $($T:ty),*) => (
        $(
            impl Cast<$T> for $F {
                #[inline(always)]
                fn cast(self) -> $T {
                    self as $T
                }
            }
        )*
    );
}

macro_rules! trait_primitive_cast {
    ($($F:ty),*) => (
        $(
            trait_cast!($F, i8, i16, i32, i64, isize, u8, u16, u32, u64, usize, f32, f64);
        )*
    );
}

trait_primitive_cast!(i8, i16, i32, i64, isize, u8, u16, u32, u64, usize, f32, f64);


#[test]
fn test() {
    assert_eq!(Cast::<isize>::cast(2_usize) * 2_isize, 4_isize);
    assert_eq!(Cast::<f32>::cast(2_u32) * 2_f32, 4_f32);
}
